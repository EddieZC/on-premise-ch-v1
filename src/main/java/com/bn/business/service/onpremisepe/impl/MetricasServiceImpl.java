package com.bn.business.service.onpremisepe.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bn.business.service.onpremisepe.exceptions.MetricaNotFoundException;
import com.bn.business.service.onpremisepe.model.api.Metrica;
import com.bn.business.service.onpremisepe.service.MetricaService;
import com.bn.business.service.onpremisepe.utils.constants.Constants;

@Component
public class MetricasServiceImpl implements MetricaService{
	
	private static List<Metrica> Metricas = new ArrayList<>();
	private static int MetricasCount = Constants.ALL_HISTORY;
	
	static {
//		Metricas.add(new Metrica(1,1,"nombreProyecto",3,"status","release","team","review",new Date()));

//		Metricass.add(new Metricas(2,6,7,8,new Date(),new Date(), true));
//		Metricass.add(new Metricas(3,10,11,12,new Date(),new Date(), true));
//		Metricass.add(new Metricas(4,6,5,4,new Date(),new Date(), true));
//		Metricass.add(new Metricas(5,21,30,40,new Date(),new Date(), true));
//		Metricass.add(new Metricas(6,22,4,55,new Date(),new Date(), true));
//		Metricass.add(new Metricas(7,45,76,7,new Date(),new Date(), true));
//		Metricass.add(new Metricas(8,28,39,40,new Date(),new Date(), true));
//		Metricass.add(new Metricas(9,21,31,41,new Date(),new Date(), true));
//		Metricass.add(new Metricas(10,27,39,40,new Date(),new Date(), true));
	}
	

	@Override
	public Metrica getHistoryMetricasById(int Id) {
		Iterator<Metrica> iterator = Metricas.iterator();
		while(iterator.hasNext()) {
			Metrica Metricas = iterator.next();
			if(Metricas.getId() == Id) {
				return Metricas;
			}
	
		}
		throw new MetricaNotFoundException("Resource id-" + Id);	
	}

}
