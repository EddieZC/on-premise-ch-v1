package com.bn.business.service.onpremisepe.expose.web;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bn.business.service.onpremisepe.config.MetricaConfig;
import com.bn.business.service.onpremisepe.model.api.Metrica;
import com.bn.business.service.onpremisepe.model.backend.MetricaRepository;
import com.bn.business.service.onpremisepe.service.MetricaService;

@RestController
@RequestMapping("/v1")
public class MetricaRestController {
	
	@Autowired
	private MetricaService service;
	
	@Autowired
	private MetricaRepository MetricaRepository;
	
	@Autowired
	private MetricaConfig config;
	
	// GET /Metricas - HTTP CODE 200
	@GetMapping(value="/metrica", headers="X-PERSISTENCE=db")											// Headers Versioning
	public ResponseEntity<List<Metrica>> getHistoryMetricasV2(){
		return ResponseEntity.ok(MetricaRepository.findAll());
			
	}
	 
	// GET /Metricas - HTTP CODE 200
	@GetMapping("/metrica/{id}")
	public ResponseEntity<Object> getHistoryMetricasByIdV1(@PathVariable int Id){

		if (Id > config.getDatabase())
				return ResponseEntity.ok(MetricaRepository.findById(Id));
		
		return null;
	}
	
		
	//POST /Metricas - HTTP CODE 201
	@PostMapping(value="/proyecto",headers="X-PERSISTENCE=db")
	public ResponseEntity<Object> saveMetricaV2(@Valid @RequestBody Metrica Metrica){

				Metrica r = MetricaRepository.save(Metrica);
				
				URI location = ServletUriComponentsBuilder
						.fromCurrentRequest()
						.path("/{Id}")
						.buildAndExpand(r.getId()).toUri();
				
				return ResponseEntity.created(location).build();

		}

	
//	//PUT /Metricas/{Id}/ - HTTP CODE 200
//	@PutMapping("/Metricas/{Id}")
//	public ResponseEntity<Object> updateMetricaById(@PathVariable int Id, @RequestBody Metrica new_destination) {
//		
//		
//		//MetricaRepository.save(MetricaRepository.findById(Id).get().setDestination_station_id(new_destination.getDestination_station_id()));
//		Metrica r = MetricaRepository.getOne(Id);
//		MetricaRepository.save(Object.class);
//		(r.setDestination_station_id(new_destination.getDestination_station_id()));
//
//		return (ResponseEntity<Object>) ResponseEntity.ok();
//		
//	}
	
	
	//DELETE /Metricas/{Id}/ - HTTP CODE 204
	@DeleteMapping(value="/metrica/{Id}",headers="X-PERSISTENCE=db")
	public ResponseEntity<Object> deleteMetricaByIdV2(@PathVariable int Id) {
			
			MetricaRepository.deleteById(Id);
			
			return ResponseEntity.noContent().build();
			
		}
	
}
