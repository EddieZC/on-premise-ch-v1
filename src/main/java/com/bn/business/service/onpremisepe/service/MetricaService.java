package com.bn.business.service.onpremisepe.service;

import com.bn.business.service.onpremisepe.model.api.Metrica;

public interface MetricaService {

	Metrica getHistoryMetricasById(int Id);

}
